# FaST Platform / addons / arc


Additional module that implements Estonian services used for soil data retrieval and fertilization recommendations.

This repository includes the [source code](services) and [a descriptive orchestration configuration](manifests) of the entire package.

## Services

### Fertilization

- [fertilization/arc](services/fertilization/arc)

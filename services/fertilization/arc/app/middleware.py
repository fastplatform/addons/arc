from starlette.middleware.base import BaseHTTPMiddleware

from app.db.graphql_clients import GraphQLClient


class GraphQLClientMiddleware(BaseHTTPMiddleware):
    """Middleware that injects the graphql clients in each request
    """
    def __init__(self, app, graphql_client: GraphQLClient):
        super().__init__(app)
        self.graphql_client = graphql_client

    async def dispatch(self, request, call_next):
        if not hasattr(request.state, "graphql_clients"):
            request.state.graphql_clients = {}
        request.state.graphql_clients[self.graphql_client.name] = self.graphql_client
        response = await call_next(request)
        return response

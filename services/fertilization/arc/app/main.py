import os
import logging

from fastapi import FastAPI
from fastapi.middleware import Middleware

import graphene
from starlette_graphene3 import GraphQLApp

from app.db.graphql_clients import (
    external,
    fastplatform
)

from app.middleware import GraphQLClientMiddleware

from app.settings import config
from app.tracing import Tracing

from app.api.query import Query
from app.api.mutation import Mutation


# FastAPI
app = FastAPI()

# GraphQL root
app.add_route(
    "/graphql",
    GraphQLApp(
        schema=graphene.Schema(query=Query, mutation=Mutation, auto_camelcase=False)
    ),
)

# Middlewares
app.add_middleware(GraphQLClientMiddleware, graphql_client=external)
app.add_middleware(GraphQLClientMiddleware, graphql_client=fastplatform)

# OpenTelemetry
Tracing.init(app)

# Log
logger = logging.getLogger(__name__)


@app.on_event("startup")
async def startup():
    logger.debug(config)
    await fastplatform.connect()
    await external.connect()


@app.on_event("shutdown")
async def shutdown():
    await fastplatform.close()
    await external.close()

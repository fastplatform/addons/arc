import os
import sys

from pathlib import Path, PurePath

from pydantic import BaseSettings

from typing import Dict, Union


class Settings(BaseSettings):

    API_DIR: Path = PurePath(__file__).parent / 'api'

    API_GATEWAY_EXTERNAL_URL: str
    API_GATEWAY_FASTPLATFORM_URL: str
    API_GATEWAY_SERVICE_KEY: str

    OPENTELEMETRY_EXPORTER_JAEGER_SERVICE_NAME: str = "iacs-arpea"
    OPENTELEMETRY_EXPORTER_JAEGER_AGENT_HOSTNAME: str = "127.0.0.1"
    OPENTELEMETRY_EXPORTER_JAEGER_AGENT_PORT: int = 5775
    OPENTELEMETRY_SAMPLING_RATIO: float = 0.1

    EPSG_SRID_ETRS89: str = "4258"
    EPSG_SRID_ETRS89_LAEA: str = "3035"
    EPSG_SRID_WGS84: str = "4326"

    ARC_SOIL_SERVICE_URL: str = "https://52.169.4.148:8080/rest/GetParcelSoilIndicator/parcelIdentifier=16561169,16561168&parcelCentroid=%02%03&userIDCode=123&dataFrom=2010-12-31/results.xml"
    ARC_FERTILIZATION_SERVICE_URL: str = ""

    LOG_COLORS = bool(os.getenv("LOG_COLORS", sys.stdout.isatty()))
    LOG_FORMAT = str(os.getenv("LOG_FORMAT", "uvicorn"))
    LOG_LEVEL = str(os.getenv("LOG_LEVEL", "info")).upper()
    LOGGING_CONFIG: Dict[str, Union[Dict, bool, int, str]] = {
        "version": 1,
        "disable_existing_loggers": False,
        "formatters": {
            "simple": {
                "class": "logging.Formatter",
                "format": "%(levelname)-10s %(message)s",
            },
            "uvicorn": {
                "()": "uvicorn.logging.DefaultFormatter",
                "format": "%(levelprefix)s %(message)s",
                "use_colors": LOG_COLORS,
            },
        },
        "handlers": {
            "default": {
                "class": "logging.StreamHandler",
                "formatter": LOG_FORMAT,
                "level": LOG_LEVEL,
                "stream": "ext://sys.stdout",
            }
        },
        "root": {"handlers": ["default"], "level": LOG_LEVEL},
        "loggers": {
            "fastapi": {"propagate": True},
            "uvicorn": {"propagate": True},
            "uvicorn.access": {"propagate": True},
            "uvicorn.asgi": {"propagate": True},
            "uvicorn.error": {"propagate": True},
        },
    }

    #### PDF GENERATOR
    PDF_GENERATOR_ENDPOINT = str(os.getenv("PDF_GENERATOR_ENDPOINT", "http://0.0.0.0:7010/generate_pdf"))

    #### ALGORITHM NAME
    ALGORITHM_NAME = str(os.getenv("ALGORITHM_NAME", "PMK"))


config = Settings()

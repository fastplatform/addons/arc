from fastapi import FastAPI

from opentelemetry import propagators, trace
from opentelemetry.exporter.jaeger import JaegerSpanExporter
from opentelemetry.instrumentation.fastapi import FastAPIInstrumentor
from opentelemetry.propagators import inject
from opentelemetry.sdk.trace import TracerProvider
from opentelemetry.sdk.trace.export import BatchExportSpanProcessor
from opentelemetry.sdk.trace.propagation.b3_format import B3Format
from opentelemetry.sdk.trace.sampling import ParentBased, TraceIdRatioBased

from app.settings import config


class Tracing():
    @staticmethod
    def init(app: FastAPI):

        exporter = JaegerSpanExporter(
            service_name=config.OPENTELEMETRY_EXPORTER_JAEGER_SERVICE_NAME,
            agent_host_name=config.
            OPENTELEMETRY_EXPORTER_JAEGER_AGENT_HOSTNAME,
            agent_port=config.OPENTELEMETRY_EXPORTER_JAEGER_AGENT_PORT,
        )

        sampler = TraceIdRatioBased(config.OPENTELEMETRY_SAMPLING_RATIO)

        trace.set_tracer_provider(TracerProvider(sampler=ParentBased(sampler)))
        trace.get_tracer_provider().add_span_processor(
            BatchExportSpanProcessor(exporter))

        propagators.set_global_textmap(B3Format())

        FastAPIInstrumentor.instrument_app(app)
    
    @staticmethod
    def inject_context(headers: dict):
        inject(Tracing._setter, headers)

    @staticmethod
    def _setter(carrier: dict, key: str, value: str):
        carrier[key] = value

import graphene

from app.api.mutations.insert_soil import InsertSoil


class Mutation(graphene.ObjectType):
    arc__insert_soil = InsertSoil.Field()

import graphene

from app.api.queries.plot_fertilization_recommendations import PlotFertilizationRecommendations
from app.api.queries.fertilization_crops import FertilizationCrops
from app.api.queries.pdf_generator import PDFGenerator


class Query(FertilizationCrops, PlotFertilizationRecommendations, PDFGenerator, graphene.ObjectType):
    arc__healthz = graphene.String(default_value="ok")
    
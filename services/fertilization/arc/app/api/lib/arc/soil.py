import requests
from requests.models import Response
from datetime import datetime, timezone
from xml.etree.ElementTree import Element
import xml.etree.ElementTree as ET
from typing import Optional

from app.api.lib.arc.common import ARCParcel
from app.settings import config


def transform_point_coords_to_geojson(coords: [float], crs: int = 4326) -> dict:
    """Transform point coordinates to geojson

    Args:
        coords ([float]): Coordinates of the point as a list of two float 
            (Example : [24.5643, 5.9756])
        crs (int, optional): Coordinate reference system as epsg identifier. 
            Defaults to 4326.

    Returns:
        dict: The point in geojson format
    """
    return {
        "coordinates": coords,
        "type": 'Point',
        "crs": {
            "type": "name",
            "properties": {
                "name": "epsg:" + str(crs)
            }
        }
    }

def convert_date_utc_to_datetime(date_utc: str) -> datetime:
    """Convert UTC date to datetime 

    Args:
        date_utc (str): Date as string with Z identifier like 'yyyy-mm-ddZ'

    Returns:
        datetime: Datetime with timezone UTC
    """

    year, month, day_z = date_utc.split('-')
    day = day_z[:2]

    year, month, day = [int(element) for element in [year, month, day]]
    
    return datetime(year, month, day).replace(tzinfo=timezone.utc)


def get_text_or_none(xml_element: Element) -> Optional[str]:
    """Get text of an Xml element

    Args:
        xml_element (Element): An element of an Xml tree 

    Returns:
        Optional[str]: Text of the Xml element if exists. Else None.
    """
    return None if xml_element is None else xml_element.text

class ARCParcelSoilIndicatorObservations:

    def __init__(self,
                 ph: float = None,
                 phosphorus: float = None,
                 potassium: float = None,
                 calcium: float = None,
                 magnesium: float = None,
                 copper: float = None,
                 manganese: float = None,
                 boron: float = None,
                 organic_carbon: float = None,
                 nitrogen: float = None):
        """Initialization of ARCParcelSoilIndicatorObservations

        Args:
            ph (float, optional): Parcel’s soil average pH level. 
                Defaults to None.
            phosphorus (float, optional): Parcel’s average phosphorus amount in 
                the soil. Defaults to None.
            potassium (float, optional): Parcel’s average potassium amount in 
                the soil. Defaults to None.
            calcium (float, optional): Parcel’s average calcium amount in 
                the soil. Defaults to None.
            magnesium (float, optional): Parcel’s average magnesium amount in 
                the soil. Defaults to None.
            copper (float, optional): Parcel’s average copper amount in the 
                soil. Defaults to None.
            manganese (float, optional): Parcel’s average manganese amount in 
                the soil. Defaults to None.
            boron (float, optional): Parcel’s average boron amount in the 
                soil. Defaults to None.
            organic_carbon (float, optional): Parcel’s average organic carbon 
                amount in the soil. Defaults to None.
            nitrogen (float, optional): Parcel’s average nitrogen amount in the 
                soil. Defaults to None.
        """
    
        self.ph = ph
        self.phosphorus = phosphorus
        self.potassium = potassium
        self.calcium = calcium
        self.magnesium = magnesium
        self.copper = copper
        self.manganese = manganese
        self.boron = boron
        self.organic_carbon = organic_carbon
        self.nitrogen = nitrogen


class ARCParcelSoilIndicator:

    ARC_EPSG = 4326

    def __init__(self,
                 identifier: str, 
                 centroid_longitude: str, 
                 centroid_latitude: str,
                 last_modified: datetime,
                 error_code: str,
                 error_text: str,
                 observations: ARCParcelSoilIndicatorObservations,
                 sample_date: datetime = None):
        """Initialization of ARCParcelSoilIndicator

        Args:
            identifier (str): Parcel identifier from ARIB register.
            centroid_longitude (str): Parcel centroid longitude coordinate in 
                WGS84 format.
            centroid_latitude (str): Parcel centroid latitude coordinate in 
                WGS84 format.
            last_modified (datetime): The last data update time.
            error_code (str): Error codes e.g. NO_DATA, SYSTEM_ERROR etc.
            error_text (str): Error description.
            observations (float): Soil observations
            sample_date (datetime, optional): Date of the soil sampling. Defaults 
                to None.
        """
    
        self.identifier = identifier
        self.centroid_longitude = centroid_longitude
        self.centroid_latitude = centroid_latitude
        self.last_modified = last_modified
        self.error_code = error_code
        self.error_text = error_text
        self.observations = observations
        self.sample_date = sample_date
        self.geometry = transform_point_coords_to_geojson(
            coords=[centroid_longitude, centroid_latitude],
            crs=ARCParcelSoilIndicator.ARC_EPSG
        )


class ARCParcelSoilIndicatorResponse:

    def __init__(self,
                 user_id_code: str,
                 parcels_soil_indicators: [ARCParcelSoilIndicator]):
        """ Initialization of ARCParcelSoilIndicatorResponse

        Args:
            user_id_code (str): User ID code, e.g. EE37208090354. 
                User is the parcel owner or parcel user.
            parcels_soil_indicators ([ARCParcelSoilIndicator]): Can be empty 
                if there is no data for that user. One parcel can exist many 
                times in the list (different samples).
        """

        self.user_id_code = user_id_code
        self.parcels_soil_indicators = parcels_soil_indicators

    def __str__(self):
        return str({
            'user_id_code': self.user_id_code,
            'parcels_soil_indicators': [
                parcel.__dict__
                for parcel in self.parcels_soil_indicators
            ]
        })


class SoilIndicatorResponseParser:

    namespace = '{http://www.pb.com/spectrum/services/GetParcelSoilIndicator}'

    def __init__(self, xml_string):
        """Initialization of SoilIndicatorResponseParser

        Args:
            xml_string (str): Xml document containing the ARC soil service 
                response as string. 
        """
        self.xml_string = xml_string
        self.xml_tree = ET.fromstring(xml_string)
        self.output = self.xml_tree.find(
            f'{SoilIndicatorResponseParser.namespace}Output'
        )
        self.data = self.output.find(
            f'{SoilIndicatorResponseParser.namespace}data'
        )
        self.response = SoilIndicatorResponseParser.parse_data(self)

    def parse_data(self) -> ARCParcelSoilIndicatorResponse:
        """Transform Xml data to ARCParcelSoilIndicatorResponse

        Returns:
            ARCParcelSoilIndicatorResponse: An object containing the user ID
                and the soil indicators of each parcel.
        """

        data = self.data
        ns = SoilIndicatorResponseParser.namespace
        arc_soil_response = ARCParcelSoilIndicatorResponse(
            user_id_code = data.find(f'{ns}userIDCode').text,
            parcels_soil_indicators = []
        )

        parcels = data.find(f'{ns}Parcels')
        for parcel in parcels:
            arc_soil_response.parcels_soil_indicators.append(
                ARCParcelSoilIndicator(
                    identifier=parcel.find(f'{ns}parcelIdentifier').text,
                    centroid_longitude=float(
                        parcel.find(f'{ns}parcelCentroidLongitude').text
                    ),
                    centroid_latitude=float(
                        parcel.find(f'{ns}parcelCentroidLatitude').text
                    ),
                    last_modified = convert_date_utc_to_datetime(
                        parcel.find(f'{ns}lastModified').text
                    ),
                    error_code = get_text_or_none(parcel.find(f'{ns}errorCode')),
                    error_text = get_text_or_none(parcel.find(f'{ns}errorText')),
                    observations = ARCParcelSoilIndicatorObservations(
                        ph = get_text_or_none(parcel.find(f'{ns}pHLevel')),
                        phosphorus = get_text_or_none(parcel.find(f'{ns}amountP')),
                        potassium = get_text_or_none(parcel.find(f'{ns}amountK')),
                        calcium = get_text_or_none(parcel.find(f'{ns}amountCa')),
                        magnesium = get_text_or_none(parcel.find(f'{ns}amountMg')),
                        copper = get_text_or_none(parcel.find(f'{ns}amountCu')),
                        manganese = get_text_or_none(parcel.find(f'{ns}amountMn')),
                        boron = get_text_or_none(parcel.find(f'{ns}amountB')),
                        organic_carbon = get_text_or_none(parcel.find(f'{ns}amountOrgC')),
                        nitrogen = get_text_or_none(parcel.find(f'{ns}amountN'))
                    ),
                    sample_date = convert_date_utc_to_datetime(
                        get_text_or_none(parcel.find(f'{ns}sampleDate'))
                    )
                )
            )
        
        return arc_soil_response


class SoilIndicatorQuery:

    def __init__(self, 
                 user_id_code: str, 
                 parcels: [ARCParcel],
                 data_from: str = None):
        """Send query of soil indicators to ARC soil service

        Args:
            user_id_code (str): User ID code, e.g. EE37208090354. 
                User is the parcel owner or parcel user.
            parcels ([ARCParcel]): List of ARCParcel instances
            data_from (str): An UTC date as a string in yyyy-mm-ddZ format 
                (example : '2019-06-01Z'). If specified, then the query result 
                contains only the parcels where last modif >= data_from
        """
        self.user_id_code = user_id_code
        self.parcels = parcels
        self.data_from = data_from
        self.xml = SoilIndicatorQuery.build_xml(self)
        self.xml_string = ET.tostring(self.xml).decode("utf-8")
    
    def build_xml(self) -> Element:
        """Build xml document for soil service based on user id and parcels data

        Returns:
            Element: Xml document as Element object from xml module
        """

        soil_query = ET.Element('xml.GetParcelSoilIndicatorQuery')
        soil_query.set(
            'xmlns',
            'http://www.pb.com/spectrum/services/GetParcelSoilIndicator'
        )

        input_el = ET.SubElement(soil_query, 'Input')
        data = ET.SubElement(input_el, 'data')

        user_id_code = ET.SubElement(data, 'userIDCode')
        user_id_code.text = self.user_id_code
        data_from = ET.SubElement(data, 'dataFrom')
        data_from.text = self.data_from

        parcels = ET.SubElement(data, 'Parcels')

        for arc_parcel in self.parcels:

            parcel = ET.SubElement(parcels, 'Parcel')

            identifier = ET.SubElement(parcel, 'parcelIdentifier')
            identifier.text = arc_parcel.identifier
            latitude = ET.SubElement(parcel, 'parcelCentroidLatitude')
            latitude.text = arc_parcel.centroid_latitude
            longitude = ET.SubElement(parcel, 'parcelCentroidLongitude')
            longitude.text = arc_parcel.centroid_longitude

        return soil_query
    
    def write_xml(self, filename: str) -> None:
        """Write xml to a file

        Args:
            filename (str): Path of the file you want to write
        """
        with open(filename, "w+") as f:
            f.write(self.xml_string)
    
    def send_query(self) -> Response:
        """Send query to ARC soil service

        Returns:
            Response: Response from ARC soil service
        """
        return requests.post(config.ARC_SOIL_SERVICE_URL, 
                             headers={'Content-Type': 'application/xml'},
                             data=self.xml_string)
    
    def __str__(self):
        return str({
            'url': config.ARC_SOIL_SERVICE_URL,
            'headers': {'Content-Type': 'text/xml'},
            'data': self.xml_string
        })


if __name__ == '__main__':

    soil_indicator_query = SoilIndicatorQuery(
        user_id_code='123abc',
        data_from='2019-06-01Z',
        parcels=[ARCParcel(identifier='ee12345', 
                            centroid_latitude='58.656', 
                            centroid_longitude='24.708'),
                    ARCParcel(identifier='ee6789', 
                            centroid_latitude='58.678', 
                            centroid_longitude='24.765')]
    )
    print("\n##### Soil indicator query #####\n")
    print(soil_indicator_query)

    #xml = soil_indicator_query.send_query().content
    xml_path = '../../../docs/GetParcelSoilIndicator.xml'
    with open(xml_path, 'r') as file:
        xml = file.read().replace('\n', '')
    soil_indicator = SoilIndicatorResponseParser(xml)
    print("\n##### Soil indicator response #####\n")
    print(soil_indicator.response)

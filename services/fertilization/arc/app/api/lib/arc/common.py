class ARCParcel:
    def __init__(
        self, identifier: str, centroid_longitude: str, centroid_latitude: str
    ):
        """Initialization of ARCParcel

        Args:
            identifier (str): Parcel identifier from ARIB register.
            centroid_longitude (str): Parcel centroid longitude coordinate in
                WGS84 format. (epsg 4326)
            centroid_latitude (str): Parcel centroid latitude coordinate in
                WGS84 format (epsg 4326).
        """

        self.identifier = identifier
        self.centroid_longitude = centroid_longitude
        self.centroid_latitude = centroid_latitude

import requests
from requests.models import Response
from datetime import datetime, timezone
from xml.etree.ElementTree import Element
import xml.etree.ElementTree as ET
from typing import Optional

from app.api.lib.arc.common import ARCParcel
from app.settings import config


class FertilizationRecoQuery:

    def __init__(self, 
                 user_id_code: str, 
                 parcels: [ARCParcel]):
        """Send fertilization recommendation query to ARC fertilization service

        Args:
            user_id_code (str): User ID code, e.g. EE37208090354. 
                User is the parcel owner or parcel user.
            parcels ([ARCParcel]): List of ARCParcel instances
        """
        self.user_id_code = user_id_code
        self.parcels = parcels
        self.xml = FertilizationRecoQuery.build_xml(self)
        self.xml_string = ET.tostring(self.xml).decode("utf-8")
    
    def build_xml(self) -> Element:
        """Build xml document for fertilization service based on user id and 
            parcels data

        Returns:
            Element: Xml document as Element object from xml module
        """

        fertilization_query = ET.Element('xml.GetParcelsFertRecQuery')
        fertilization_query.set('xmlns','http://www.pb.com/spectrum/services/GetParcelsFertRec')

        input_el = ET.SubElement(fertilization_query, 'Input')
        data = ET.SubElement(input_el, 'data')

        user_id_code = ET.SubElement(data, 'userIDCode')
        user_id_code.text = self.user_id_code

        parcels = ET.SubElement(data, 'Parcels')

        for arc_parcel in self.parcels:

            parcel = ET.SubElement(parcels, 'Parcel')

            identifier = ET.SubElement(parcel, 'parcelIdentifier')
            identifier.text = arc_parcel.identifier
            latitude = ET.SubElement(parcel, 'parcelCentroidLatitude')
            latitude.text = arc_parcel.centroid_latitude
            longitude = ET.SubElement(parcel, 'parcelCentroidLongitude')
            longitude.text = arc_parcel.centroid_longitude

        return fertilization_query
    
    def write_xml(self, filename: str) -> None:
        """Write xml to a file

        Args:
            filename (str): Path of the file you want to write
        """
        with open(filename, "w+") as f:
            f.write(self.xml_string)
    
    def send_query(self) -> Response:
        """Send query to ARC soil service

        Returns:
            Response: Response from ARC soil service
        """
        return requests.post(config.ARC_FERTILIZATION_SERVICE_URL, 
                             headers={'Content-Type': 'application/xml'},
                             data=self.xml_string)
    
    def __str__(self):
        return str({
            'url': config.ARC_FERTILIZATION_SERVICE_URL,
            'headers': {'Content-Type': 'application/xml'},
            'data': self.xml_string
        })

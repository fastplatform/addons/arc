from functools import partial
from pyproj import (
    Proj,
    Transformer,
)
from shapely import ops
from shapely.geometry import shape, Point

from app.settings import config


def reproject_function(source_epsg, target_epsg):

    source = Proj(f'EPSG:{source_epsg}')
    target = Proj(f'EPSG:{target_epsg}')

    transform = Transformer.from_proj(source, target, always_xy=True).transform

    return partial(ops.transform,transform)

class Projection:

    # EPSG:4258 -> EPSG:4326
    etrs89_to_wsg84 = reproject_function(config.EPSG_SRID_ETRS89, config.EPSG_SRID_WGS84)

    # EPSG:4326 -> EPSG:4258
    wsg84_to_etrs_89 = reproject_function(config.EPSG_SRID_ETRS89, config.EPSG_SRID_WGS84)


def add_crs(geojson_without_crs, epsg=4326):
    geojson_without_crs["crs"] = {
        "type": "name",
        "properties": {
        "name": f"urn:ogc:def:crs:EPSG::{epsg}"
        }
    }

    return geojson_without_crs
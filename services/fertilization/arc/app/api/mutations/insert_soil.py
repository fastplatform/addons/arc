import graphene
import json
from datetime import datetime
import logging
import random

from gql import gql
import xmltodict

from shapely.geometry import Point, shape

from app.api.types.common import PlotInputType
from app.api.types.soil import ARCSoilResponseType

from app.api.lib.arc.soil import SoilIndicatorQuery, SoilIndicatorResponseParser
from app.api.lib.arc.common import ARCParcel
from app.api.lib.gis import Projection, add_crs

from app.settings import config

logger = logging.getLogger(__name__)


class InsertSoil(graphene.Mutation):
    class Arguments:
        holding_id = graphene.String(required=True)
        data_from = graphene.DateTime(required=False)
        plot_ids = graphene.List(graphene.NonNull(graphene.Int), required=False)

    class Meta:
        name = "arc__insert_soil_response"

    ok = graphene.NonNull(graphene.Boolean)
    errors = graphene.String()
    plot_ids = graphene.List(graphene.NonNull(graphene.Int))

    async def mutate(self, info, holding_id, data_from, plot_ids):

        # Load the mappings for soil observable properties
        soil_observable_properties = json.loads(
            (
                config.API_DIR / "data/arc_soil_observable_property_mapping.json"
            ).read_text()
        )

        request = info.context["request"]

        if plot_ids is None:
            query = (
                config.API_DIR / "graphql/query_plot_by_holding_id.graphql"
            ).read_text()
            variables = {
                "holding_id": holding_id,
                "user_id": request.headers["X-Hasura-User-Id"],
            }
        else:
            query = (
                config.API_DIR / "graphql/query_plot_by_holding_id_and_plot_ids.graphql"
            ).read_text()
            variables = {
                "holding_id": holding_id,
                "plot_ids": plot_ids,
                "user_id": request.headers["X-Hasura-User-Id"],
            }

        client = request.state.graphql_clients["fastplatform"]
        response = await client.execute(gql(query), variables)

        if plot_ids:
            # We might be requesting plots that do not exist or are not permitted for this user to see
            plot_ids_not_found = [
                plot_id
                for plot_id in plot_ids
                if plot_id not in [p["id"] for p in response["plot"]]
            ]
            if plot_ids_not_found:
                logger.warning(
                    f"Plot IDs {plot_ids_not_found} have not been found or are not accessible to this user"
                )

        # Replace the requested plot list by the plots we are actually allowed to see
        plots = {p["authority_id"]: p for p in response["plot"]}

        # Prepare the query to the ARC endpoint
        # TODO
        pass

        # Send the query
        # TODO remove that once the endpoints are operational
        response = (
            config.API_DIR / "lib/arc/samples/GetParcelSoilIndicator.xml"
        ).read_text()

        # Convert the query response to a dict
        data = xmltodict.parse(
            response,
            process_namespaces=False,
            postprocessor=lambda path, key, value: (key.lower(), value),
            force_list=("parcel",),
        )
    
        items = data.get("xml.getparcelsoilindicatorresponse", {})
        items = items.get("output", {})
        items = items.get("data", {})
        items = items.get("parcels", {})
        items = items.get("parcel", [])

        objects = []

        for item in items:
            plot_authority_id = item["parcelidentifier"]

            # TODO Remove that once the connection is operational
            plot_authority_id = random.choice(list(plots.keys()))

            plot = plots.get(plot_authority_id, None)
            if plot is None:
                logger.warning(
                    f"Received unexpected plot_authority_id={plot_authority_id}, ignoring it"
                )
                continue

            sample_date = item.get("sampledate", None)
            if sample_date is not None:
                sample_date = datetime.strptime(sample_date, '%Y-%m-%dZ').isoformat()

            obj = {
                "authority_id": plot_authority_id,
                "valid_from": sample_date,
                "valid_to": None,
                "holding_id": holding_id,
                "soil_investigation_purpose_id": "http://inspire.ec.europa.eu/codelist/SoilInvestigationPurposeValue/generalSoilSurvey",
                "source": json.dumps(item),
                "observations": {
                    "data": [],
                    "on_conflict": {
                        "constraint": "observation_soil_site_observed_property_phenomenon_time_unique",
                        "update_columns": ["result"],
                    },
                },
            }

            geometry = Point(
                float(item["parcelcentroidlongitude"]),
                float(item["parcelcentroidlatitude"]),
            )
            geometry = Projection.wsg84_to_etrs_89(geometry)
            geometry = geometry.__geo_interface__
            geometry["crs"] = {
                "type": "name",
                "properties": {"name": f"EPSG:{config.EPSG_SRID_ETRS89}"},
            }

            obj["geometry"] = geometry

            for (
                observable_property_id,
                arc_property_id,
            ) in soil_observable_properties.items():

                value = item.get(arc_property_id.lower(), None)
                if value is None:
                    continue

                obj["observations"]["data"] += [{
                    "result": {"value": float(value)},
                    "phenomenon_time": sample_date,
                    "observed_property_id": observable_property_id,
                }]

            objects += [obj]

        print(objects)

        mutation = (
            config.API_DIR / "graphql/mutation_insert_soil_sites.graphql"
        ).read_text()
        variables = {"objects": objects}

        client = request.state.graphql_clients["fastplatform"]
        response = await client.execute(gql(mutation), variables)

        print(objects)
        print(response)

        return InsertSoil(
            ok=True,
            plot_ids=[p["id"] for p in plots.values()],
        )

import json

import graphene

from app.api.types.fertilization import CropType
from app.settings import config


class FertilizationCrops(graphene.ObjectType):

    arc__fertilization_crops = graphene.List(graphene.NonNull(CropType))

    def resolve_arc__fertilization_crops(self, info):

        arc_crop_mapping = (config.API_DIR / "data/arc_crop_mapping.json").read_text()
        arc_crop_mapping = json.loads(arc_crop_mapping)
        return [
            CropType(
                id=hash((
                    arc_id,
                    tuple(plant_species_ids)
                )),
                arc_id=arc_id,
                plant_species_ids=plant_species_ids,
            )
            for arc_id, plant_species_ids in arc_crop_mapping.items()
        ]
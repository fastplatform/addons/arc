import logging
from datetime import datetime
import json
import random

import graphene
import xmltodict
from gql import gql

from shapely.geometry import Point

from app.api.types.fertilization import (
    PlotFertilizationRecommendationType,
    PlantSpeciesYieldRecommendationType,
    ChemicalElementRecommendationType,
)

from app.api.lib.gis import Projection
from app.settings import config

logger = logging.getLogger(__name__)


class PlotFertilizationRecommendations(graphene.ObjectType):

    arc__plot_fertilization_recommendations = graphene.List(
        graphene.NonNull(PlotFertilizationRecommendationType),
        plot_ids=graphene.Argument(
            graphene.List(graphene.NonNull(graphene.Int)), required=True
        ),
    )

    async def resolve_arc__plot_fertilization_recommendations(self, info, plot_ids):
        """Resolver for `arc__plot_fertilization_recommendations` node

        Arguments:
            info {object} -- GraphQL context
            user_id {str} -- User ID (Estonian EE1234567890 code)
            plot_authority_ids {[str]} -- List of plot authority IDs (ie the plot ID in the PRIA db)

        Returns:
            [PlotFertilizationRecommendationType] -- List of plot fertilization recommendations
        """

        request = info.context["request"]

        # Load the mappings for crops and chemical elements
        plant_species_mapping = json.loads(
            (config.API_DIR / "data/arc_crop_mapping.json").read_text()
        )
        chemical_element_mapping = json.loads(
            (config.API_DIR / "data/arc_chemical_element_mapping.json").read_text()
        )

        # Get the authority_ids and centroids of the plots from the FaST database, using this user
        # This will also filter out plots that the user is requesting but is in fact not allowed to see
        # TODO this should be replaced by a call using the Hasura "farmer" role instead of the "service" role
        # so that Hasura applies the permissions
        query = (
            config.API_DIR / "graphql/query_plot_centroids_and_authority_ids.graphql"
        ).read_text()
        variables = {
            "plot_ids": plot_ids,
            "user_id": request.headers["X-Hasura-User-Id"],
        }
        client = request.state.graphql_clients["fastplatform"]
        response = await client.execute(gql(query), variables)

        # We might be requesting plots that do not exist or are not permitted for this user to see
        plot_ids_not_found = [
            plot_id
            for plot_id in plot_ids
            if plot_id not in [p["id"] for p in response["plot"]]
        ]
        if plot_ids_not_found:
            logger.warning(
                f"Plot IDs {plot_ids_not_found} have not been found or are not accessible to this user"
            )

        # Replace the requested plot list by the plots we are actually allowed to see
        plots = {p["authority_id"]: p for p in response["plot"]}

        # Prepare the query to the ARC endpoint
        # TODO
        pass

        # Send the query
        # TODO remove that once the endpoints are operational
        response = (
            config.API_DIR / "lib/arc/samples/GetParcelFertilizationRecommendations.xml"
        ).read_text()

        # Convert the query response to a dict
        data = xmltodict.parse(
            response,
            process_namespaces=False,
            postprocessor=lambda path, key, value: (key.lower(), value),
            force_list=("parcel", "recommendation", "chemicalelement"),
        )

        items = data.get("xml.getparcelsfertrecresponse", {})
        items = items.get("output", {})
        items = items.get("row", {})
        items = items.get("parcels", {})
        items = items.get("parcel", [])

        response = {}

        # Convert each item we received into its equivalent GraphQL node type
        for item in items:

            # The parcelIdentifierString received from ARC corresponds to thge authority_id
            # in FaST
            plot_authority_id = item["parcelidentifierstring"]

            # TODO Remove that once the connection is operational
            plot_authority_id = random.choice(list(plots.keys()))

            # Check that we do have a plot corresponding to this authority_id we have received
            plot = plots.get(plot_authority_id, None)
            if plot is None:
                logger.warning(
                    f"Received unexpected plot_authority_id={plot_authority_id}, ignoring it"
                )
                continue

            # If this plot has not yet been initialized in our response, do it and compute
            # some plot-level attributes
            if plot["id"] not in response:

                # The geometry we received is WGS84, reproject to ETRS89 (as per FaST data model)
                geometry = Point(
                    float(item["parcelcentroidlongitude"]),
                    float(item["parcelcentroidlatitude"]),
                )
                geometry = Projection.wsg84_to_etrs_89(geometry)
                geometry = geometry.__geo_interface__
                geometry["crs"] = {
                    "type": "name",
                    "properties": {"name": f"EPSG:{config.EPSG_SRID_ETRS89}"},
                }

                ph_level = item.get("parcelphlevel", None)
                if ph_level is not None:
                    ph_level = float(ph_level)

                sample_date = item.get("sampledate", None)
                if sample_date is not None:
                    sample_date = datetime.strptime(sample_date, '%Y-%m-%dZ').isoformat()

                response[plot["id"]] = PlotFertilizationRecommendationType(
                    plot_id=plot["id"],
                    ph_level=ph_level,
                    sample_date=sample_date,
                    centroid=json.dumps(geometry),
                    recommendations=[],
                )

            recommendations = item.get("recommendations", {})
            recommendations = recommendations.get("recommendation", [])

            for recommendation in recommendations:
                plant_species_yield_recommendation = (
                    PlantSpeciesYieldRecommendationType(
                        plant_species_ids=plant_species_mapping[recommendation["crop"]],
                        yield_target=float(
                            recommendation["yieldtarget"].replace("t", "")
                        )
                        * 1000,  # Convert to kg/ha
                        chemical_elements=[],
                    )
                )

                chemical_elements = recommendation.get("chemicalelements", {})
                chemical_elements = chemical_elements.get("chemicalelement", [])
                for chemical_element in chemical_elements:
                    chemical_element_recommendation = ChemicalElementRecommendationType(
                        chemical_element_id=chemical_element_mapping[
                            chemical_element["elementid"]
                        ],
                        quantity=float(chemical_element["elementamount"]),
                    )

                    # For each element, compute an id as a hash of its attributes
                    chemical_element_recommendation.id = hash(
                        (
                            chemical_element_recommendation.chemical_element_id,
                            chemical_element_recommendation.quantity,
                        )
                    )
                    plant_species_yield_recommendation.chemical_elements.append(
                        chemical_element_recommendation
                    )

                # For each element, compute an id as a hash of its attributes
                plant_species_yield_recommendation.id = hash(
                    (
                        tuple(plant_species_yield_recommendation.plant_species_ids),
                        plant_species_yield_recommendation.yield_target,
                        (
                            ce.id
                            for ce in plant_species_yield_recommendation.chemical_elements
                        ),
                    )
                )

                response[plot["id"]].recommendations.append(
                    plant_species_yield_recommendation
                )

            for plot_id, plot_fertilization_recommendation in response.items():

                # For each element, compute an id as a hash of its attributes
                plot_fertilization_recommendation.id = hash(
                    (
                        plot_fertilization_recommendation.plot_id,
                        plot_fertilization_recommendation.ph_level,
                        plot_fertilization_recommendation.sample_date,
                        plot_fertilization_recommendation.centroid,
                        (
                            r.id
                            for r in plot_fertilization_recommendation.recommendations
                        ),
                    )
                )

        return response.values()

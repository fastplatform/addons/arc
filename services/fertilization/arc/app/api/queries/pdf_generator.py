import graphene
import requests
import base64

from opentelemetry import trace

from app.api.types.pdf import PDFGeneratorInputType, PDFGeneratorOutputType
from app.settings import config
from app.tracing import Tracing


# OpenTelemetry
def tracer():
    return trace.get_tracer(__name__)

class PDFGenerator(graphene.ObjectType):

    arc__generate_results_pdf = graphene.NonNull(
        PDFGeneratorOutputType,
        input=graphene.Argument(
            PDFGeneratorInputType,
            required=True
        )
    )

    def resolve_arc__generate_results_pdf(self, info, input):
        try:
            inputs = input['inputs']
            # read the results html template
            with open(config.API_DIR / 'templates/pdf/base.html', 'r') as f:
                template = f.read()
            inputs['algorithm_name'] = config.ALGORITHM_NAME
            try:
                # get algo logo as static image
                with open(config.API_DIR / 'static/logo_arc.png', 'rb') as f:
                    logo = f.read()
                    inputs['algorithm_logo_base64'] = 'data:image/png;base64,' + base64.b64encode(logo).decode()
            except Exception as e:
                print('FAILED TO GET ALGORITHM LOGO', str(e))
            with tracer().start_as_current_span("generate_pdf"):
                # generate PDF
                headers = {}
                Tracing.inject_context(headers)
                res = requests.post(config.PDF_GENERATOR_ENDPOINT, headers=headers, json={
                    "template": template,
                    "data": inputs 
                })
                if (res.status_code == 200):
                    try:
                        # base64_content = base64.b64encode(res.content).decode()
                        return PDFGeneratorOutputType(
                            pdf=res.content
                        )
                    except Exception as e:
                        print('Error while base64 encoding pdf content: {}'.format(str(e)))
                        raise Exception(e)
                else:
                    raise Exception('REQUEST TO PDF GENERATOR FAILED')
        except Exception as e:
            print('[ERROR] PDF generation error: {}'.format(str(e)))
            raise Exception("error while generating results pdf")

import graphene


class PlotInputType(graphene.InputObjectType):
    authority_id = graphene.String()
    centroid = graphene.Argument(graphene.String, required=True)

    class Meta:
        name = 'arc__plot_input'

import graphene


class PDFGeneratorInputType(graphene.InputObjectType):
    inputs = graphene.JSONString(required=True)

    class Meta:
        name = 'arc__pdf_generator_input'

class PDFGeneratorOutputType(graphene.ObjectType):
    pdf = graphene.Base64()

    class Meta:
        name = 'arc__pdf_generator_output'

import graphene


class ParcelSoilIndicatorType(graphene.ObjectType):
    id = graphene.String()
    identifier = graphene.ID()
    centroid_longitude = graphene.String()
    centroid_latitude = graphene.String()
    last_modified = graphene.DateTime()
    error_code = graphene.String()
    error_text = graphene.String()
    ph_level = graphene.Float()
    amount_p = graphene.Float()
    amount_k = graphene.Float()
    amount_ca = graphene.Float()
    amount_mg = graphene.Float()
    amount_cu = graphene.Float()
    amount_mn = graphene.Float()
    amount_b = graphene.Float()
    amount_org_c = graphene.Float()
    amount_n = graphene.Float()
    sample_date = graphene.DateTime()

    class Meta:
        name = "arc__parcel_soil_indicator"

class ARCSoilResponseType(graphene.ObjectType):
    id = graphene.String()
    user_id_code = graphene.String()
    parcels_soil_indicators = graphene.List(ParcelSoilIndicatorType)

    class Meta:
        name = "arc__soil_response_type"

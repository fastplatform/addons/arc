import graphene

class ChemicalElementRecommendationType(graphene.ObjectType):
    id = graphene.String()
    chemical_element_id = graphene.String()
    quantity = graphene.Float()

    class Meta:
        name = 'arc__chemical_element_recommendation'

class PlantSpeciesYieldRecommendationType(graphene.ObjectType):
    id = graphene.String()
    plant_species_ids = graphene.List(graphene.String)
    yield_target = graphene.Float()
    chemical_elements = graphene.List(ChemicalElementRecommendationType)

    class Meta:
        name = 'arc__plant_species_yield_recommendation'

class PlotFertilizationRecommendationType(graphene.ObjectType):
    id = graphene.String()
    plot_id = graphene.Int()
    centroid = graphene.String()
    ph_level = graphene.Float()
    sample_date = graphene.DateTime()
    recommendations = graphene.List(PlantSpeciesYieldRecommendationType)

    class Meta:
        name = 'arc__plot_fertilization_recommendations'

class CropType(graphene.ObjectType):
    id = graphene.String()
    arc_id = graphene.String()
    plant_species_ids = graphene.List(graphene.String)

    class Meta:
        name = 'arc__crop'

# fertilization/arc

```fertilization/arc``` is a [FastAPI](https://fastapi.tiangolo.com/) service that exposes endpoints that provide:
-  **fertilization recommendation** with data fetched from the ARC API.
-  **manage Soil data** ,fetched from the ARC API, and compliant with the Soil Inspire ontology.

The above endpoints are called by the [fastplatform](https://gitlab.com/fastplatform/core/-/tree/master/services/api_gateway/fastplatform) Hasura API Gateway when selected events are triggered on farm plots.

## Prerequisites

- Python 3.7+ and [`virtualenv`](https://virtualenv.pypa.io/en/latest/)

## Environment variables

- `ALGORITHM_NAME`: name of the algorithm as displayed on the fertilization plan report
- `API_GATEWAY_EXTERNAL_URL`: URL of the [external](https://gitlab.com/fastplatform/core/-/tree/master/services/api_gateway/external) Hasura API Gateway
- `API_GATEWAY_FASTPLATFORM_URL`: URL of the [fastplatform](https://gitlab.com/fastplatform/core/-/tree/master/services/api_gateway/fastplatform) Hasura API Gateway
- `API_GATEWAY_SERVICE_KEY`: secret servive key to access [external](https://gitlab.com/fastplatform/core/-/tree/master/services/api_gateway/external) and [fastplatform](https://gitlab.com/fastplatform/core/-/tree/master/services/api_gateway/fastplatform) Hasura API Gateways
- `ARC_API_KEY`: API key to access the ARC services
- `LOG_LEVEL`: logging level (trace, debug, info, warn, error, fatal)


## Development Setup

Create a Python virtualenv and activate it:
```
virtualenv .venv
source .venv/bin/activate
```

Install Python dependencies:
```
pip install -r requirements.txt

# Optionally install dev packages
pip install -r requirements-dev.txt
```

If needed start monitoring OpenTelemetry traces
```
make start-tracing
```
Jaeger UI will be available at [http://localhost:16686]()

Start the service:
```
make start
```

The API server is now started and available at http://localhost:7010.

**Server can be also started with Bazel** (no need to activate a Python virtualenv):
```bash
$ make bazel-run 
```

## Sample 

GraphQL queries are included in the [tests](tests) folder.
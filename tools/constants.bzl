PY37_IMAGE_BASE = select({
    "@io_bazel_rules_docker//:debug": "@py3.7_debug_image_base//image",
    "@io_bazel_rules_docker//:fastbuild": "@py3.7_image_base//image",
    "@io_bazel_rules_docker//:optimized": "@py3.7_image_base//image",
    "//conditions:default": "@py3.7_image_base//image",
})

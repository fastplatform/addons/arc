#!/bin/bash

git_rev=$(git rev-parse HEAD 2>/dev/null)
echo "BUILD_SCM_REVISION ${git_rev}"

git diff-index --quiet HEAD -- 2>/dev/null
if [[ $? == 0 ]];
then
    tree_status="Clean"
else
    tree_status="Modified"
fi
echo "BUILD_SCM_STATUS ${tree_status}"

GITSHA=$(git describe --always 2>/dev/null)

ARC_VERSION=${ARC_VERSION:=$(grep 'ARC_VERSION\s*=' VERSION | awk '{print $3}')}
ARC_CI_VERSION=${ARC_CI_VERSION:=$(grep 'ARC_CI_VERSION\s*=' VERSION | awk '{print $3}')}

if [[ -z "${VERSION}" ]]; then
  if [[ -z "${CI}" ]]; then
    VERSION=${ARC_VERSION}
  else
    VERSION="${ARC_CI_VERSION}+${GITSHA}"
  fi
fi

echo "STABLE_ARC_VERSION ${VERSION}"

ARC_TAG=${VERSION/+/-}
echo "STABLE_ARC_TAG ${ARC_TAG}"
echo "STABLE_ARC_TAG_PREFIXED_WITH_COLON :${ARC_TAG}"

if [[ -z "${DOCKER_REGISTRY}" ]]; then
  DOCKER_REGISTRY="pwcfasteu.azurecr.io"
fi
if [[ -z "${DOCKER_IMAGE_PREFIX}" ]]; then
  DOCKER_IMAGE_PREFIX=fastplatform/addons/arc/
fi
echo "STABLE_DOCKER_REGISTRY ${DOCKER_REGISTRY}"
echo "STABLE_DOCKER_IMAGE_PREFIX ${DOCKER_IMAGE_PREFIX}"

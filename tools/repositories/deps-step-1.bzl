load("@bazel_tools//tools/build_defs/repo:utils.bzl", "maybe")
load("@io_bazel_rules_docker//container:container.bzl", "container_pull")
load("@io_bazel_rules_docker//python3:image.bzl", _py_image_repos = "repositories")
load("@io_bazel_rules_docker//repositories:deps.bzl", container_deps = "deps")
load("@rules_python_external//:defs.bzl", "pip_install")

def transitive_deps():
    _py_image_repos()

def docker_deps():
    container_deps()
    maybe(
        container_pull,
        name = "py3.7_image_base",
        registry = "gcr.io",
        repository = "distroless/python3-debian10",
        digest = "sha256:f7d590fed7404ad6fcf6199012de4ea1dcefc93393c85d64783f8737009715b4",
    )
    maybe(
        container_pull,
        name = "py3.7_debug_image_base",
        registry = "gcr.io",
        repository = "distroless/python3-debian10",
        digest = "sha256:e918de863d04e3b5524d5c426eba05e0273cfa5673287acad7690c5337c7f55e",
    )

def pip_deps():
    maybe(
        pip_install,
        name = "fertilization_arc_pip",
        requirements = "//services/fertilization/arc:requirements.txt",
    )

def deps_step_1():
    transitive_deps()
    docker_deps()
    pip_deps()

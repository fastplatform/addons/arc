#!/bin/bash

set -o errexit
set -o nounset
set -o pipefail

source $(dirname $0)/common.sh

bazel build //:images.MANIFEST
cp $(bazel info bazel-genfiles)/images.MANIFEST .

bazel build //:query_collections.yaml
cp $(bazel info bazel-genfiles)/query_collections.yaml .

# Encrypt sensitive files
bazel build //:release_kustomize_manifest.tar.gz
cp $(bazel info bazel-genfiles)/release_kustomize_manifest.tar.gz .
